<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPrestamo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_libro')->unsigned();
            $table->integer('id_estudiante')->unsigned();
            $table->integer('id_empleado')->unsigned();
            $table->foreign('id_libro')->references('id')->on('libros');
            $table->foreign('id_estudiante')->references('id')->on('estudiantes');
            $table->foreign('id_empleado')->references('id')->on('empleados');
            $table->date('f_prestamo');
            $table->date('f_entrega');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestamo');
    }
}
