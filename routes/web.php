<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Curso\Estudiantes;

Route::get('/', function () {
    return view('estudiantes.crearEstudiante');
});

Route::resource('estudiantes', 'EstudiantesController');
Route::get('/crearEstudiante', 'EstudiantesController@create');

Route::resource('empleados', 'EmpleadosController');
Route::get('/crearEmpleado', 'EmpleadosController@create');

Route::resource('libros', 'LibrosController');
Route::get('/crearLibro', 'LibrosController@create');


Route::resource('prestamos', 'PrestamoController');
Route::get('/crearPrestamo', 'PrestamoController@create');