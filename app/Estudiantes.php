<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Estudiantes extends Model
{
    protected $table = 'estudiantes';


    protected $fillable = [
        'nombre',
        'edad',
        'carrera',
        'cedula',
        'sexo'
      ];
}
