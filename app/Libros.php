<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Libros extends Model
{
    protected $table = 'libros';


    protected $fillable = [
        'nombre',
        'editorial',
        'año',
        'ubicacion',
        'autor',
        'tipo',
        'area',
        'dias'
      ];

}
