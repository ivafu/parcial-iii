<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Prestamo extends Model
{
    protected $table = 'prestamo';


    protected $fillable = [
        'id_libro',
        'id_estudiante',
        'id_empleado',
        'f_prestamo',
        'f_entrega'
      ];

}
