<?php

namespace Sistema;

use Illuminate\Database\Eloquent\Model;

class Empleados extends Model
{
    protected $table = 'empleados';


    protected $fillable = [
        'nombre',
        'edad',
        'cedula',
        'sexo'
      ];
}
