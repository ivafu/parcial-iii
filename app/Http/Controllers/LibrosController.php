<?php

namespace Sistema\Http\Controllers;

use Sistema\Libros;
use Illuminate\Http\Request;

class LibrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $libro = Libros::all();
        

        return view('libros.index', compact('libro'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('libros.crearLibro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required|string',
            'editorial'=> 'required|string',
            'año'=>'required|integer:unsigned',
            'ubicacion'=>'required|string',
            'autor'=>'required|string',
            'tipo'=>'required|string',
            'area'=>'required|string',
            'dias'=>'required|integer',
            
          ]);
          
          $libro = new Libros([
            'nombre'=>$request->get('nombre'),
            'editorial'=>$request->get('editorial'),
            'año'=>$request->get('año'),
            'ubicacion'=>$request->get('ubicacion'),
            'autor'=>$request->get('autor'),
            'tipo'=>$request->get('tipo'),
            'area'=>$request->get('area'),
            'dias'=>$request->get('dias'),

            
            
          ]);
          $libro->save();
          return redirect('/libros')->with('success', 'Stock has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Sistema\Libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function show(Libros $libros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Sistema\Libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $libro = Libros::find($id);
        return view('libros.edit',compact('libro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Sistema\Libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'nombre'=>'required|string',
            'editorial'=> 'required|string',
            'año'=>'required|integer:unsigned',
            'ubicacion'=>'required|string',
            'autor'=>'required|string',
            'tipo'=>'required|string',
            'area'=>'required|string',
            'dias'=>'required|integer',
            
          ]);
    
          $libro = Libros::find($id);
          $libro->nombre= $request->get('nombre');
          $libro->editorial= $request->get('editorial');
          $libro->año=$request->get('año');
          $libro->ubicacion=$request->get('ubicacion');
          $libro->autor=$request->get('autor');
          $libro->tipo= $request->get('tipo');
          $libro->area=$request->get('area');
          $libro->dias=$request->get('dias');

          
          $libro->save();
    
          return redirect('/libros')->with('success', 'Stock has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Sistema\Libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $libro = Libros::find($id);
        $libro->delete();

        return redirect('/libros')->with('success', 'Stock has been deleted Successfully');
    }
}
