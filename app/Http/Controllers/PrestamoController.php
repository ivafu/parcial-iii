<?php

namespace Sistema\Http\Controllers;

use Sistema\prestamo;
use Sistema\estudiantes;
use Sistema\libros;
use Sistema\empleados;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PrestamoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prestamo = Prestamo::all();
        

        return view('prestamos.index', compact('prestamo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estudiante = Estudiantes::all();
        $empleado = Empleados::all();
        $libro = Libros::all();

        return view('prestamos.crearPrestamo', compact('estudiante','empleado','libro'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $prestamo = new Prestamo([
            'id_libro'=>$request->get('id_libro'),
            'id_estudiante'=>$request->get('id_estudiante'),
            'id_empleado'=>$request->get('empleado'),
            'f_prestamo'=>$request->get('f_prestamo'),
            'f_entrega'=>$request->get('f_entrega'),
            

            
            
          ]);
          $prestamo->save();
          return redirect('/prestamos')->with('success', 'Stock has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Sistema\prestamo  $prestamo
     * @return \Illuminate\Http\Response
     */
    public function show(prestamo $prestamo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Sistema\prestamo  $prestamo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Sistema\prestamo  $prestamo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, prestamo $prestamo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Sistema\prestamo  $prestamo
     * @return \Illuminate\Http\Response
     */
    public function destroy(prestamo $prestamo)
    {
        //
    }
}
