<?php

namespace Sistema\Http\Controllers;

use Sistema\Empleados;
use Illuminate\Http\Request;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleado = Empleados::all();
        

        return view('empleados.index', compact('empleado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empleados.crearEmpleado');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required|string',
            'edad'=> 'required|integer:unsigned',
            'cedula'=>'required|integer',
            'sexo'=>'required|string',
            
          ]);
          $empleado = new Empleados([
            'nombre'=>$request->get('nombre'),
            'edad'=>$request->get('edad'),
            'cedula'=>$request->get('cedula'),
            'sexo'=>$request->get('sexo'),
            
            
          ]);
          $empleado->save();
          return redirect('/empleados')->with('success', 'Stock has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Sistema\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function show(Empleados $empleados)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Sistema\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empleado = Empleados::find($id);
        return view('empleados.edit',compact('empleado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Sistema\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre'=>'required|string',
            'edad'=> 'required|integer:unsigned',
            'cedula'=>'required|integer',
            'sexo'=>'required|string',
            
          ]);
    
          $empleado = Empleados::find($id);
            $empleado->nombre = $request->get('nombre');
            $empleado->edad= $request->get('edad');
            $empleado->cedula= $request->get('cedula');
            $empleado->sexo= $request->get('sexo');
       
          $empleado->save();
    
          return redirect('/empleados')->with('success', 'Stock has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Sistema\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empleado = Empleados::find($id);
        $empleado->delete();

        return redirect('/empleados')->with('success', 'Stock has been deleted Successfully');
    }
}
