<?php

namespace Sistema\Http\Controllers;

use Sistema\estudiantes;
use Illuminate\Http\Request;

class EstudiantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudiante = Estudiantes::all();
        

        return view('estudiantes.index', compact('estudiante'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estudiantes.crearEstudiante');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required|string',
            'edad'=> 'required|integer:unsigned',
            'carrera'=>'required|string',
            'cedula'=>'required|integer',
            'sexo'=>'required|string',
            
          ]);
          $estudiante = new Estudiantes([
            'nombre'=>$request->get('nombre'),
            'edad'=>$request->get('edad'),
            'carrera'=>$request->get('carrera'),
            'cedula'=>$request->get('cedula'),
            'sexo'=>$request->get('sexo'),
            
            
          ]);
          $estudiante->save();
          return redirect('/estudiantes')->with('success', 'Stock has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Sistema\estudiantes  $estudiantes
     * @return \Illuminate\Http\Response
     */
    public function show(estudiantes $estudiantes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Sistema\estudiantes  $estudiantes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estudiante = Estudiantes::find($id);
        return view('estudiantes.edit',compact('estudiante'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Sistema\estudiantes  $estudiantes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
          $request->validate([
            'nombre'=>'required|string',
            'edad'=> 'required|integer:unsigned',
            'carrera'=>'required|string',
            'cedula'=>'required|integer',
            'sexo'=>'required|string',
            
          ]);
    
          $estudiante = Estudiantes::find($id);
            $estudiante->nombre = $request->get('nombre');
            $estudiante->edad= $request->get('edad');
            $estudiante->carrera= $request->get('carrera');
            $estudiante->cedula= $request->get('cedula');
            $estudiante->sexo= $request->get('sexo');
       
          $estudiante->save();
    
          return redirect('/estudiantes')->with('success', 'Stock has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Sistema\estudiantes  $estudiantes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estudiante = Estudiantes::find($id);
        $estudiante->delete();

        return redirect('/estudiantes')->with('success', 'Stock has been deleted Successfully');
    }
}
