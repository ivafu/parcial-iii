@extends('layouts.layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Añadir libro
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('libros.store') }}">
          <div class="form-group">
              @csrf
              <label for="nombre">Nombre:</label>
              <input type="text" class="form-control" name="nombre"/>
          </div>
          <div class="form-group">
              <label for="editorial">Editorial: </label>
              <input type="text" class="form-control" name="editorial"/>
          </div>
          <div class="form-group">
            <label for="año">Año: </label>
            <input type="number" class="form-control" name="año"/>
        </div>
        <div class="form-group">
            <label for="ubicacion">Ubicacion: </label>
            <input type="text" class="form-control" name="ubicacion"/>
        </div>
        <div class="form-group">
            <label for="autor">Autor: </label>
            <input type="text" class="form-control" name="autor"/>
        </div>
        <div class="form-group">
            <label for="tipo">Tipo :</label>
            <input type="radio" name="tipo" value="normal" checked> Normal
            <input type="radio" name="tipo" value="reserva"> Reserva<br>
        </div>
       <div class="form-group">
        <label for="area">Area de conocimiento :</label>
        <input type="text" class="form-control" name="area"/>
      </div>
      <div class="form-group">
        <label for="dias">Dias de prestamo :</label>
        <input type="number" class="form-control" name="dias"/>
      </div>
      
         
          <button type="submit" class="btn btn-primary">Añadir</button>
      </form>
  </div>
</div>
@endsection