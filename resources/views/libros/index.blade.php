@extends('layouts.layout')
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Nombre</td>
          <td>Editorial</td>
          <td>Año</td>
          <td>Ubicacion</td>
          <td>Autor</td>
          <td>Tipo</td>
          <td>Area de conocimiento</td>
          <td>Dias de prestamo</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($libro as $l)
        <tr>
            <td>{{$l->id}}</td>
            <td>{{$l->nombre}}</td>
            <td>{{$l->editorial}}</td>
            <td>{{$l->año}}</td>
            <td>{{$l->ubicacion}}</td>
            <td>{{$l->autor}}</td>
            <td>{{$l->tipo}}</td>
            <td>{{$l->area}}</td>
            <td>{{$l->dias}}</td>
            
            
            <td><a href="{{ route('libros.edit',$l->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('libros.destroy', $l->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection