@extends('layouts.layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Share
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('libros.update', $libro->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
            <label for="nombre">Nombre:</label>
            <input type="text" class="form-control" name="nombre" value={{ $libro->nombre }} />
          </div>
          <div class="form-group">
            <label for="editorial">Editorial :</label>
            <input type="text" class="form-control" name="editorial" value={{ $libro->editorial }} />
          </div>
          <div class="form-group">
              <label for="año">Año :</label>
              <input type="number" class="form-control" name="año" value={{ $libro->año }} />
            </div>
            <div class="form-group">
              <label for="ubicacion">Ubicacion :</label>
              <input type="text" class="form-control" name="ubicacion" value={{ $libro->ubicacion }} />
            </div>
            <div class="form-group">
                <label for="autor">Autor :</label>
                <input type="text" class="form-control" name="autor" value={{ $libro->autor }} />
              </div>
            <div class="form-group">
                <label for="tipo">Tipo :</label>
                <input type="radio" name="tipo" value="normal" checked> Normal
                <input type="radio" name="tipo" value="reserva"> Reserva<br>
            </div>
            <div class="form-group">
                <label for="area">Area de conocimiento:</label>
                <input type="text" class="form-control" name="area" value={{ $libro->area }} />
              </div>
           <div class="form-group">
                <label for="dias">Dias de prestamo:</label>
                <input type="number" class="form-control" name="dias" value={{ $libro->dias }} />
              </div>  

    
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@endsection