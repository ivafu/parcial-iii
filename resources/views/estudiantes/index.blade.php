@extends('layouts.layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Nombre</td>
          <td>Edad</td>
          <td>Carrera</td>
          <td>Cedula</td>
          <td>Sexo</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($estudiante as $e)
        <tr>
            <td>{{$e->id}}</td>
            <td>{{$e->nombre}}</td>
            <td>{{$e->edad}}</td>
            <td>{{$e->carrera}}</td>
            <td>{{$e->cedula}}</td>
            <td>{{$e->sexo}}</td>
            
            <td><a href="{{ route('empleados.edit',$e->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('estudiantes.destroy', $e->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection