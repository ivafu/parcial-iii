@extends('layouts.layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Añadir estudiante
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('estudiantes.store') }}">
          <div class="form-group">
              @csrf
              <label for="nombre">Nombre:</label>
              <input type="text" class="form-control" name="nombre"/>
          </div>
          <div class="form-group">
              <label for="edad">Edad: </label>
              <input type="number" class="form-control" name="edad"/>
          </div>
          <div class="form-group">
            <label for="carrera">Carrera :</label>
            <input type="text" class="form-control" name="carrera"/>
        </div>
        <div class="form-group">
          <label for="cedula">Cedula :</label>
          <input type="number" class="form-control" name="cedula"/>
      </div>
      <div class="form-group">
        <label for="Sexo">Sexo :</label>
        <input type="radio" name="sexo" value="hombre" checked> Male
        <input type="radio" name="sexo" value="mujer"> Female<br>
    </div>
         
          <button type="submit" class="btn btn-primary">Añadir</button>
      </form>
  </div>
</div>
@endsection