@extends('layouts.layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Nombre</td>
          <td>Edad</td>
          <td>Cedula</td>
          <td>Sexo</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($empleado as $em)
        <tr>
            <td>{{$em->id}}</td>
            <td>{{$em->nombre}}</td>
            <td>{{$em->edad}}</td>
            <td>{{$em->cedula}}</td>
            <td>{{$em->sexo}}</td>
            
            <td><a href="{{ route('empleados.edit',$em->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('empleados.destroy', $em->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection