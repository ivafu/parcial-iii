@extends('layouts.layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Share
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('empleados.update', $empleado->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label for="nombre">Nombre:</label>
          <input type="text" class="form-control" name="nombre" value={{ $empleado->nombre }} />
        </div>
        <div class="form-group">
          <label for="edad">Edad :</label>
          <input type="number" class="form-control" name="edad" value={{ $empleado->edad }} />
        </div>
          <div class="form-group">
            <label for="cedula">Cedula :</label>
            <input type="number" class="form-control" name="cedula" value={{ $empleado->cedula }} />
          </div>
          <div class="form-group">
            <label for="Sexo">Sexo :</label>
            <input type="radio" name="sexo" value="hombre" checked> Male
            <input type="radio" name="sexo" value="mujer"> Female<br>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@endsection