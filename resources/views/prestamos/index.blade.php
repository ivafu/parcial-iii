@extends('layouts.layout')
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Id_libro</td>
          <td>Id_estudiante</td>
          <td>Id_empleado</td>
          <td>Fecha prestamo</td>
          <td>Fecha entrega</td>
          
        </tr>
    </thead>
    <tbody>
        @foreach($prestamo as $p)
        <tr>
            <td>{{$p->id}}</td>
            <td>{{$p->id_libro}}</td>
            <td>{{$p->id_estudiante}}</td>
            <td>{{$p->id_empleado}}</td>
            <td>{{$p->f_prestamo}}</td>
            <td>{{$p->f_entrega}}</td>
        
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection