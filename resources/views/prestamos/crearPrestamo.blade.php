@extends('layouts.layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Añadir prestamo
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('prestamo.store') }}">
          <div class="form-group">
              @csrf
            
                
                <div class="form-group">
                    <label for="libro">Libro: </label>
                    <select name="id_libro">
                            @foreach($libro as $l)
                           <option value="{{$l->id}}">{{$l->nombre}}</option>
                           @endforeach
                    </select>       
                </div>     
            

            
            
            <div class="form-group">
                <label for="estudiante">Estudiante: </label>
                <select name="id_estudiante">
                    @foreach($estudiante as $e)   
                    <option value="{{$e->id}}">{{$e->nombre}}</option>
                    @endforeach
                </select>       
            </div>     
           
            
            
            <div class="form-group">
                <label for="empleado">Empleado: </label>
                <select name="empleado">
                        @foreach($empleado as $em)
                       <option value="{{$em->id}}">{{$em->nombre}}</option>
                       @endforeach
                </select>       
            </div>     
            <div class="form-group">
                    <label for="fecha">Fecha de prestamo: </label>
                    <input type="date" class="form-control" name="f_prestamo"/> 
            </div>
            <div class="form-group">
                    <label for="fecha">Fecha de entrega: </label>
                    <input type="date" class="form-control" name="f_entrega"/> 
            </div>
           

            

          <button type="submit" class="btn btn-primary">Añadir</button>
      </form>
  </div>
</div>
@endsection