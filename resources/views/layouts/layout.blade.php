<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Biblioteca</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Module') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">

                            <a class="navbar-brand" href="{{url('crearLibro')}}">Libros</a>
                            <a class="navbar-brand" href="{{url('crearEstudiante')}}">Estudiantes</a>
                            <a class="navbar-brand" href="{{url('crearEmpleado')}}">Empleados</a>
                        </li>
                    </ul>
                  </div>
                </div>
            </nav>
  <div class="container">
    @yield('content')
  </div>
</div>
  <script src="{{ asset('js/app.js') }}" defer></script>
  <script src="{{ asset('js/app.js') }}" type="text/js"></script>
</body>
</html>